package gl.engine;

import org.joml.Vector2i;

import java.util.EnumSet;

public interface GameLogic {

    void init(Window window) throws Exception;
    void update(float delta, int keys, Mouse mouse);
    void render(Window window);
    void cleanup();
}