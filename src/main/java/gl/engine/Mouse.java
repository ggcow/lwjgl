package gl.engine;

import org.joml.Vector2i;

public class Mouse {
    int x;
    int y;

    int lastX;
    int lastY;

    boolean in;
    boolean leftButtonPressed;
    boolean rightButtonPressed;

    public Vector2i getDelta() {
        int dx = x-lastX;
        int dy = y-lastY;
        lastX = x;
        lastY = y;
        return new Vector2i(dx, dy);
    }
}