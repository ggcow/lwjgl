package gl.engine;


import gl.engine.graph.Mesh;
import org.joml.Vector2i;
import org.joml.Vector3f;

import static java.lang.Math.*;

public class Player {

    private final Vector3f eye;
    private final Vector3f look;

    private float azimuth;
    private float inclination;

    private float speed;

    public Player() {
        eye = new Vector3f(0, 0, 0);
        look = new Vector3f(0, 0, 0);
        azimuth = inclination = 0;
        speed = 100;
    }

    public Vector3f getEye() {
        return new Vector3f(eye);
    }
    public void setEye(float x, float y, float z) {
        this.eye.x = x;
        this.eye.y = y;
        this.eye.z = z;
    }

    public float getSpeed() {
        return speed;
    }
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Vector3f getLook() {
        return look;
    }

    public void setLook(Vector2i delta) {
        setLook(delta.x, delta.y);
    }
    public void setLook(int dx, int dy) {
        azimuth -= dx/300f;
        inclination -= dy/300f;

        if (2*inclination+0.4f >= PI)
            inclination = (float) (PI/2f-0.2f);
        if (2*inclination-0.4f < -PI)
            inclination = (float) (0.2f-PI/2f);

        if (azimuth >= PI)
            azimuth -= 2*PI;
        if (azimuth < -PI)
            azimuth += 2*PI;

        look.set((float) (cos(inclination)*sin(azimuth)),
                (float) (sin(inclination)),
                (float) (cos(inclination)*cos(azimuth)));
    }
}