package gl.engine;
public class GameEngine implements Runnable {

    private final Window window;
    private final Timer timer;
    private final GameLogic logic;
    private final Thread game;

    public GameEngine(String windowTitle, int width, int height, boolean vSync, GameLogic logic) throws Exception {
        game = new Thread(this, "GAME_LOOP_THREAD");
        window = new Window(windowTitle, width, height, vSync);
        this.logic = logic;
        timer = new Timer();
    }

    public void start() {
        String osName = System.getProperty("os.name");
        if (osName.contains("Mac")) {
            game.run();
        } else {
            game.start();
        }
    }

    @Override
    public void run() {
        try {
            init();
            gameLoop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logic.cleanup();
            window.destroy();
        }
    }

    protected void init() throws Exception {
        window.init();
        logic.init(window);
    }

    protected void gameLoop() {
        boolean running = true;
        while (running && !window.shouldClose()) {
            update(timer.getDelta() / 1_000_000_000);
            render();
        }
    }

    protected void update(float delta) {
        logic.update(delta, window.getKeys(), window.getMouse());
    }

    protected void render() {
        logic.render(window);
        window.update();
    }
}