package gl.engine;

public class Timer {

    private long delta;
    private long time;
    private long timeFps;
    private long fps;

    public Timer() {
        time = timeFps = System.nanoTime();
        fps = 0;
        update();
    }

    private void update() {
        long temp = System.nanoTime();
        delta = temp-time;
        time = temp;

        fps++;
        if (time - timeFps > 1_000_000_000) {
            System.err.println("FPS : " + fps);
            timeFps = time;
            fps = 0;
        }
    }

    public float getDelta() {
        update();
        return  delta;
    }
}