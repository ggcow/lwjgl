package gl.game;


import gl.engine.*;
import gl.engine.graph.Mesh;
import gl.engine.graph.ShaderProgram;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;


public class Renderer {

    private ShaderProgram shaderProgram;

    private static final float FOV = (float) Math.toRadians(90.0f);
    private static final float Z_NEAR = 0.01f;
    private static final float Z_FAR = 1000.f;

    public Renderer() throws Exception {
    }

    public void init(Window window) throws Exception {
        // Create shader
        shaderProgram = new ShaderProgram();
        shaderProgram.createVertexShader(Utils.loadResource("/vertex.vs"));
        shaderProgram.createFragmentShader(Utils.loadResource("/fragment.fs"));
        shaderProgram.link();

        // Create uniforms for world and projection matrices
        shaderProgram.createUniform("MVP");

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);

        window.setClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public void render(Window window, Player player, Mesh mesh) {
        clear();

        if (window.isResized()) {
            glViewport(0, 0, window.getWidth(), window.getHeight());
            window.setResized(false);
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shaderProgram.bind();

        Matrix4f perspective = new Matrix4f().perspective(FOV, (float) window.getWidth()/window.getHeight(), Z_NEAR, Z_FAR);
        Matrix4f lookAt = new Matrix4f().lookAt(player.getEye(), player.getLook().add(player.getEye()), new Vector3f(0, 1, 0));
        Matrix4f MVP = perspective.mul(lookAt);
        shaderProgram.setUniform("MVP", MVP);
        // Render the mesh for this game item
        mesh.render();

        shaderProgram.unbind();
    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public void cleanup() {
        if (shaderProgram != null) {
            shaderProgram.cleanup();
        }
    }
}