package gl.game;

import gl.engine.GameEngine;

public class Main {

    public static void main(String[] args) {
        try {
            new GameEngine(
                    "GAME",
                    1280, 1024,
                    false,
                    new DummyGame())
                    .start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
