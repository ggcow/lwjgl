package gl.game;

import gl.engine.GameLogic;
import gl.engine.Mouse;
import gl.engine.Player;
import gl.engine.graph.Mesh;
import gl.engine.Window;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.EnumSet;

public class DummyGame implements GameLogic {

    private final Renderer renderer;


    private Player player;
    private Mesh mesh;
    private final Carte carte;

    public DummyGame() throws Exception {
        renderer = new Renderer();
        carte = new Carte(200);
    }

    @Override
    public void init(Window window) throws Exception {
        renderer.init(window);
        carte.build();
        mesh = carte.mesh;
        player = new Player();
        player.setEye(0, 0, 0);
    }

    @Override
    public void update(float delta, int keys, Mouse mouse) {
        player.setLook(mouse.getDelta());

        Vector3f look = player.getLook();
        Vector3f eye = player.getEye();

        Vector2f direction = new Vector2f(look.x, look.z);
        direction.normalize();

        boolean forward = (keys & Window.KEY_FORWARD) > 0;
        boolean backward = (keys & Window.KEY_BACKWARD) > 0;
        boolean left = (keys & Window.KEY_LEFT) > 0;
        boolean right = (keys & Window.KEY_RIGHT) > 0;
        boolean up = (keys & Window.KEY_UP) > 0;
        boolean down = (keys & Window.KEY_DOWN) > 0;

        float speed = player.getSpeed();

        if (forward ^ backward) {
            player.setEye(
                    eye.x + (forward ? 1 : -1) * direction.x * delta * speed,
                    eye.y,
                    eye.z + (forward ? 1 : -1) * direction.y * delta * speed);
        }

        if (left ^ right) {
            player.setEye(
                    eye.x + (left ? 1 : -1) * direction.y * delta * speed,
                    eye.y,
                    eye.z + (left ? -1 : 1) * direction.x * delta * speed);
        }

        if (up ^ down) {
            player.setEye(
                    eye.x,
                    eye.y + (down ? -1 : 1) * 0.5f * delta * speed,
                    eye.z);
        }
    }

    @Override
    public void render(Window window) {
        renderer.render(window, player, mesh);
    }

    @Override
    public void cleanup() {
        renderer.cleanup();
        mesh.cleanUp();
    }
}