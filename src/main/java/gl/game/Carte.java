package gl.game;

import gl.engine.graph.Mesh;
import org.joml.Vector3i;

import java.util.*;


public class Carte {
    private Set<Vector3i> cubes;
    private boolean[][][] map;

    private final int width;
    private final int length;
    private final int height;

    private float[] b; //buffer
    private int b_index = 0;
    private int[] v; //indices
    private int v_index = 0;

    Mesh mesh;

    public Carte(int size) {
        this(size, size, size);
    }
    public Carte(int width, int length, int height) {
        this.width = width|1;
        this.length = length|1;
        this.height = height|1;
        cubes = new HashSet<>();
        map = new boolean[this.width][this.height][this.length];
    }

    public void build() {
        load();
        optimize();
        mesh = new Mesh(b, v);
        unload();
        System.out.println("Map built!");
    }

    private void unload() {
        cubes = null;
        map = null;
        b = null;
        v = null;
    }

    private void optimize() {
        b = new float[8*3*cubes.size()/50];
        v = new int[6*4*cubes.size()/50];

        boolean[][] face = new boolean[3][2];
        int[][][] point = new int[2][2][2];

        int progress = 0;
        int loading = 0;
        int total = cubes.size();
        for (Vector3i cube : cubes) {
            if (10f*++progress/total > loading) {
                System.out.println("Loading map : "+(10*++loading)+"%");
            }

            int x = cube.x;
            int y = cube.y;
            int z = cube.z;

            for (int i=0; i<2; i++) {
                face[0][i] = checkCoordinates(x+(i<<1)-1, y, z) && getCube(x+(i<<1)-1, y, z);
                face[1][i] = checkCoordinates(x, y+(i<<1)-1, z) && getCube(x, y+(i<<1)-1, z);
                face[2][i] = checkCoordinates(x, y, z+(i<<1)-1) && getCube(x, y, z+(i<<1)-1);
            }

            for (int i=0; i<2; i++)
            for (int j=0; j<2; j++)
            for (int k=0; k<2; k++) {
                if (!(face[0][i] && face[1][j] && face[2][k])) {
                    point[i][j][k] = b_index/3;
                    bufferAdd((float) (x+i),
                            (float) (y+j),
                            (float) (z+k));
                }
            }

            for (int i=0; i<2; i++)
            for (int j=0; j<2; j++)
            for (int k=0; k<2; k++) {
                if ((i^(j^k))==0) {
                    if (!face[0][i]) {
                        indicesAdd(point[i][1-j][k],
                                point[i][j][1-k],
                                point[i][j][k]);
                    }
                    if (!face[1][j]) {
                        indicesAdd(point[i][j][k],
                                point[1-i][j][k],
                                point[i][j][1-k]);
                    }
                    if (!face[2][k]) {
                        indicesAdd(point[i][j][k],
                                point[1-i][j][k],
                                point[i][1-j][k]);
                    }
                }
            }
        }
    }

    private void checkIndicesSize(int length) {
        int newLength = v_index + length;
        if (newLength <= v.length)
            return;
        newLength *= 2;
        v = Arrays.copyOf(v, newLength);
    }
    private void indicesAdd(int a, int b, int c) {
        checkIndicesSize(3);
        v[v_index++] = a; v[v_index++] = b; v[v_index++] = c;
    }

    private void checkBufferSize(int length) {
        int newLength = b_index + length;
        if (newLength <= b.length)
            return;
        newLength *= 2;
        b = Arrays.copyOf(b, newLength);
    }
    private void bufferAdd(float x, float y, float z) {
        checkBufferSize(3);
        b[b_index++] = x; b[b_index++] = y; b[b_index++] = z;
    }


    private boolean checkCoordinates(int x, int y, int z) {
        return -width/2 <= x && x <= width/2
                && -length/2 <= y && y <= length/2
                && -height/2 <= z && z <= height/2;
    }

    private void load() {

        int progress = 0;
        int loading = 0;
        for (int i=-width/2; i<=width/2; i++) {
            if (10f * ++progress / width > loading) {
                System.out.println("Creating map : " + (10 * ++loading) + "%");
            }
            for (int j = -length / 2; j <= length / 2; j++)
                for (int k = -height / 2; k <= height / 2; k++) {
                    if (equation(2f * i / width, 2f * j / length, 2f * k / height)) {
                        cubes.add(new Vector3i(i, j, k));
                        putCube(i, j, k);
                    }
                }
        }}

    private void putCube(int x, int y, int z) {
        map[x+width/2][y+height/2][z+length/2] = true;
    }
    private boolean getCube(int x, int y, int z) {
        return map[x+width/2][y+height/2][z+length/2];
    }

    private boolean equation(float x, float y, float z) {
        float e = x*x-(y*y+z*z)*(y*y+z*z);
        return Math.abs(e) < 0.1f;
    }

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
}
